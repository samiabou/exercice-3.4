package fr.cnam.foad.nfa035.badges.gui.view;

import fr.cnam.foad.nfa035.badges.gui.components.BadgePanel;
import fr.cnam.foad.nfa035.badges.gui.context.ApplicationContextProvider;
import fr.cnam.foad.nfa035.badges.gui.controller.BadgeWalletController;
import fr.cnam.foad.nfa035.badges.gui.model.BadgesModel;
import fr.cnam.foad.nfa035.badges.gui.renderer.BadgeSizeCellRenderer;
import fr.cnam.foad.nfa035.badges.gui.renderer.DefaultBadgeCellRenderer;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Component("badgeWallet")
@Order(value = 2)
public class BadgeWalletGUI {

    private JButton button1;
    private JPanel panelParent;
    private JTable table1;
    private JPanel panelImageContainer;
    private JScrollPane scrollBas;
    private JScrollPane scrollHaut;
    private JPanel panelHaut;
    private JPanel panelBas;
    private BadgePanel badgePanel;


    private BadgesModel tableModel;
    private List<DigitalBadge> tableList;

    @Autowired
    private AddBadgeDialog addBadgeDialog;

    @Autowired
    private BadgeWalletController badgesWalletController;

    /**
     * Constructeur
     */
    public BadgeWalletGUI() {
    }

    @PostConstruct
    public void postConstruct(){
        button1.addActionListener(e -> {
            addBadgeDialog.pack();
            addBadgeDialog.setLocationRelativeTo(null);
            addBadgeDialog.setIconImage(addBadgeDialog.getIcon().getImage());
            addBadgeDialog.setVisible(true);
        });
        tableModel = new BadgesModel(tableList);
        badgePanel.setPreferredSize(scrollHaut.getPreferredSize());
        table1.setModel(tableModel);
        table1.setRowSelectionInterval(0, 0);

        table1.getColumnModel().getColumn(4).setCellRenderer(new BadgeSizeCellRenderer());
        table1.setDefaultRenderer(Object.class, new DefaultBadgeCellRenderer());
        table1.setDefaultRenderer(Number.class, new DefaultBadgeCellRenderer());
        table1.setDefaultRenderer(Date.class, new DefaultBadgeCellRenderer());
        table1.getRowSorter().toggleSortOrder(0);

        table1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent event) {
                super.mouseClicked(event);
                if (event.getClickCount() == 2) {
                    int row = ((JTable) event.getComponent()).getSelectedRow();
                    badgesWalletController.loadBadge(row, getInstance());
                }
            }
        });

    }

        /**
         *
         * @param badge
         */
    public void delegateSetAddedBadge(DigitalBadge badge) {
        badgesWalletController.setAddedBadge(badge, this);
    }

    /**
     *
     * @return
     */
    public ImageIcon getIcon() {
        return new ImageIcon(Objects.requireNonNull(getClass().getResource("/logo.png")));
    }

    /**
     * Commentez-moi
     */
    private void createUIComponents() {
        try {
            this.badgesWalletController = ApplicationContextProvider.getApplicationContext().getBean("badgesWalletController", BadgeWalletController.class);

            badgesWalletController.delegateUIComponentsCreation(this);
            badgesWalletController.delegateUIManagedFieldsCreation(this);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    /**
     *
     * @param tableList
     */
    public void setTableList(List<DigitalBadge> tableList) {
        this.tableList = tableList;
    }

    /**
     *
     * @return
     */

    public JPanel getPanelImageContainer() {
        return panelImageContainer;
    }

    /**
     *
     * @param panelImageContainer
     */
    public void setPanelImageContainer(JPanel panelImageContainer) {
        this.panelImageContainer = panelImageContainer;
    }

    public void setBadgePanel(BadgePanel badgePanel) {
        this.badgePanel = badgePanel;
    }

    /**
     *
     * @return BadgesModel
     */
    public BadgesModel getTableModel() {
        return tableModel;
    }

    /**
     *
     *
     * @return JTable
     */
    public JTable getTable1() {
        return table1;
    }

    /**
     *
     *Getter
     * @return JScrollPane
     */
    public JScrollPane getScrollHaut() {
        return scrollHaut;
    }

    /**
     *
     *Getter
     * @return JPanel
     */
    public JPanel getPanelHaut() {
        return panelHaut;
    }

    /**
     * Getter
     *
     * @return List<DigitalBadge>
     */
    public List<DigitalBadge> getTableList() {
        return tableList;
    }

    /**
     * Getter
     *
     * @return BadgeWalletGUI
     */
    private BadgeWalletGUI getInstance() {
        return this;
    }

    /**
     *
     * Getter
     * @return panelParent
     */
    public JPanel getPanelParent() {
        return panelParent;
    }

}








