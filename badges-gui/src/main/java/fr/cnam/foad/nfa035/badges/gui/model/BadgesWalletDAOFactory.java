package fr.cnam.foad.nfa035.badges.gui.model;

import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.DirectAccessBadgeWalletDAOImpl;
import org.springframework.beans.factory.config.AbstractFactoryBean;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component("badgesWalletDAO")
@Order(value = 1)
public class BadgesWalletDAOFactory extends AbstractFactoryBean<DirectAccessBadgeWalletDAO> {
    private static final String RESOURCES_PATH = "badges-gui/src/main/resources/";

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public Class<?> getObjectType() {
        return DirectAccessBadgeWalletDAOImpl.class;
    }

    /**
     * {@inheritDoc}
     * @return
     * @throws IOException
     */
    @Override
    protected DirectAccessBadgeWalletDAO createInstance() throws IOException {
        return new DirectAccessBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet_indexed.csv");
    }
}

