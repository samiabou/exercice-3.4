package fr.cnam.foad.nfa035.badges.gui.view;


import fr.cnam.foad.nfa035.badges.gui.controller.AddBadgeDialogController;
import org.jdesktop.swingx.JXDatePicker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.Objects;

@Component("addBadgeDialog")
@Order(2)

public class AddBadgeDialog extends JDialog implements PropertyChangeListener {

    private JButton buttonOK;
    private JButton buttonCancel;
    private JPanel contentPane;
    private JTextField codeSerie;
    private JXDatePicker dateDebut;
    private JXDatePicker dateFin;
    private JFileChooser fileChooser;


    @Autowired
    private DisplayedBadgeHolder displayedBadgeHolder;

    @Autowired
    private AddBadgeDialogController addBadgeController;

    @Autowired
    private BadgeWalletGUI badgeWallet;


    public AddBadgeDialog() {

        fileChooser.setFileFilter(new FileNameExtensionFilter("Petites images *.jpg,*.png,*.jpeg,*.gif", "jpg", "png", "jpeg", "gif"));
        fileChooser.addPropertyChangeListener(this);
        codeSerie.addPropertyChangeListener(this);
        dateDebut.addPropertyChangeListener(this);
        dateFin.addPropertyChangeListener(this);

        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        contentPane.registerKeyboardAction(e -> onCancel(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onCancel() {
        dispose();
    }

    /**
     * getter
     * @return CodeSerie
     */
    public JTextField getCodeSerie() {
        return codeSerie;
    }

    /**
     * Getter
     * @return DateDebut
     */
    public JXDatePicker getDateDebut() {
        return dateDebut;
    }

    /**
     * Getter
     * @return DateFin
     */

    public JXDatePicker getDateFin() {
        return dateFin;
    }

    /**
     * Getter
     * @return fileChooser
     */

    public JFileChooser getFileChooser() {
        return fileChooser;
    }

    @PostConstruct
    public void postConstruct() {
        setContentPane(contentPane);
    }



    private void onOK() {
        try {
            addBadgeController.delegateOnOk(displayedBadgeHolder.getDisplayedBadge());
            badgeWallet.delegateSetAddedBadge(displayedBadgeHolder.getDisplayedBadge());
        } catch (IOException ioException) {
            ioException.printStackTrace();
            dispose();

    }

    }

    public static void main(String[] args) {
        AddBadgeDialog dialog = new AddBadgeDialog();
        dialog.pack();
        dialog.setLocationRelativeTo(null);
        dialog.setIconImage(dialog.getIcon().getImage());
        dialog.setVisible(true);
        System.exit(0);
    }

    public ImageIcon getIcon() {
        return new ImageIcon(Objects.requireNonNull(getClass().getResource("/logo.png")));
    }

    /**
     * {@inheritDoc}
     * @param evt
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        this.buttonOK.setEnabled(addBadgeController.validateForm(this));

    }



}
