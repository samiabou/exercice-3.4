package fr.cnam.foad.nfa035.badges.gui.controller;


import fr.cnam.foad.nfa035.badges.gui.BadgeWalletGUI;
import fr.cnam.foad.nfa035.badges.gui.components.BadgePanel;
import fr.cnam.foad.nfa035.badges.gui.components.BadgesPanelFactory;
import fr.cnam.foad.nfa035.badges.gui.model.BadgesModel;
import fr.cnam.foad.nfa035.badges.gui.view.DisplayedBadgeHolder;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;


@Component("badgesWalletController")
public class BadgeWalletController {

    @Autowired
    private BadgesPanelFactory badgesPanelFactory;

    @Autowired
    private DirectAccessBadgeWalletDAO dao;

    @Autowired
    private DisplayedBadgeHolder displayedBadgeHolder;


    /**
     *
     * @param view
     * @throws IOException
     */
    public void delegateUIComponentsCreation(BadgeWalletGUI view) throws IOException {

        Set<DigitalBadge> metaSet = new TreeSet<>(dao.getWalletMetadata());
        List<DigitalBadge> tableList = new ArrayList<>();
        tableList.addAll(metaSet);
        view.setTableList(tableList);
        displayedBadgeHolder.setDisplayedBadge(tableList.get(0));

    }

    /**
     *
     * @param view
     */

    public void delegateUIManagedFieldsCreation(BadgeWalletGUI view) {
        BadgePanel badgePanel = badgesPanelFactory.createInstance();
        badgePanel.setPreferredSize(new Dimension(256, 256));
        view.setBadgePanel(badgePanel);
        JPanel panelImageContainer = new JPanel();
        view.setPanelImageContainer(panelImageContainer);
        panelImageContainer.add(badgePanel);


    }

    /**
     *
     * @param row
     * @param view
     */
    public void loadBadge(int row, BadgeWalletGUI view){

        displayedBadgeHolder.setDisplayedBadge(view.getTableModel().getBadges().get(row));

        JPanel panelHaut = view.getPanelHaut();
        JScrollPane scrollHaut = view.getScrollHaut();

        delegateUIManagedFieldsCreation(view);
        JPanel panelImageContainer = view.getPanelImageContainer();

        panelHaut.removeAll();
        panelHaut.revalidate();

        view.getTable1().setRowSelectionInterval(row, row);
        panelHaut.add(scrollHaut);
        panelImageContainer.setPreferredSize(new Dimension(256, 256));
        scrollHaut.setViewportView(panelImageContainer);

        panelHaut.repaint();

    }

    /**
     *
     * @param badge
     * @param view
     */
    public void setAddedBadge(DigitalBadge badge, BadgeWalletGUI view) {
        BadgesModel tableModel = view.getTableModel();
        tableModel.addBadge(badge);
        tableModel.fireTableDataChanged();
        loadBadge(tableModel.getRowCount()-1, view);


    }

    public void delegateUIComponentsCreation(fr.cnam.foad.nfa035.badges.gui.view.BadgeWalletGUI badgeWalletGUI) {
    }
}