package fr.cnam.foad.nfa035.badges.gui.renderer;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.util.Date;

public class DefaultBadgeCellRenderer extends DefaultTableCellRenderer {

    Color originForeground = null;

    /**
     * Constructeur
     */

    public DefaultBadgeCellRenderer() {
        super();
        this.originForeground = this.getForeground();
    }

    /**
     *
     * @param table
     * @param value
     * @param isSelected
     * @param hasFocus
     * @param row
     * @param column
     * @return
     */
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component comp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus,    row, column);

        if (((Date)table.getModel().getValueAt(row, 3)).before(new Date())){
            setForeground(Color.RED);
        }
        else{
            setForeground(this.originForeground);
        }

        return comp;
    }

}