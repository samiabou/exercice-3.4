package fr.cnam.foad.nfa035.badges.gui.components;


import fr.cnam.foad.nfa035.badges.gui.view.DisplayedBadgeHolder;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component("badgePanelFactory")
@Order(value = 1)
public class BadgesPanelFactory {

    private static final String RESOURCES_PATH = "badges-gui/src/main/resources/";

    @Autowired
    DisplayedBadgeHolder displayedBadgeHolder;

    @Autowired
    DirectAccessBadgeWalletDAO dao;

    /**
     *
     * @return
     */

    public BadgePanel createInstance() {
        return new BadgePanel(displayedBadgeHolder.getDisplayedBadge(), dao);
    }

}
