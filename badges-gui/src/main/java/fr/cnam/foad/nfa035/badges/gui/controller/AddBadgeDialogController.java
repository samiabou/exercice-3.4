package fr.cnam.foad.nfa035.badges.gui.controller;


import fr.cnam.foad.nfa035.badges.gui.AddBadgeDialog;
import fr.cnam.foad.nfa035.badges.gui.view.DisplayedBadgeHolder;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.Date;


@Component("addBadgeController")
@Order(1)

public class AddBadgeDialogController {

    @Autowired
    private DirectAccessBadgeWalletDAO dao;

    @Autowired
    private DisplayedBadgeHolder displayedBadgeHolder;

    /**
     *
     * @param badge
     * @throws IOException
     */

    public void delegateOnOk(DigitalBadge badge) throws IOException {
        dao.addBadge(badge);
    }

    /**
     *
     * @param view
     * @return
     */
    public boolean validateForm(AddBadgeDialog view) {

            return false;
        }


    }


